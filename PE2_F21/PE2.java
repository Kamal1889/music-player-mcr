package PE2_F21;


import java.io.*;
import java.util.*; 

import javax.sound.midi.Instrument;
import javax.sound.midi.MidiChannel;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.Synthesizer;

import org.junit.jupiter.api.Test;


/* PLEASE DO NOT MODIFY A SINGLE STATEMENT IN THE TEXT BELOW.
READ THE FOLLOWING CAREFULLY AND FILL IN THE GAPS

I hereby declare that all the work that was required to 
solve the following problem including designing the algorithms
and writing the code below, is solely my own and that I received
no help in creating this solution and I have not discussed my solution 
with anybody. I affirm that I have read and understood
the Senate Policy on Academic honesty at 
https://secretariat-policies.info.yorku.ca/policies/academic-honesty-senate-policy-on/
and I am well aware of the seriousness of the matter and the penalties that I will face as a 
result of committing plagiarism in this assignment.

BY FILLING THE GAPS,YOU ARE SIGNING THE ABOVE STATEMENTS.

Full Name: Kamaljit Grewal
Student Number: 218 159 483
Course Section: A
*/



public class PE2 {

	public static void main(String[] args) {
		// Remember to remove any code that you may write in the main method, before you submit.	
//		@Test
//		void test() throws MismatchException {
			List<String> musicSheet = PerformingArt.readFile("music3");
			Music arrangement = new Music("MCR", "Black Parade", musicSheet);
			arrangement.play();
//		}
	} // end of main
} // end of PE2

	interface Art extends Comparable<Art> {  //extends Comparable<Art> because we should be able to compare Note-to-Note-to-Art, and compare Music-To-Music-to-Art
		public void play() throws MismatchException;  // Art objects, including Music and Note can be played! define this in Art interface for consistency
		//throws MismatchException because invalid Notes will not play, will instead throw exception
	}
	
	
	abstract class PerformingArt implements Art {  
		//CANNOt create a PerformingArt object because it is abstract.
		//is extended by Music and Note, and those 2 classes inherit exceptionCode and play(). keeps things consistent
		static int exceptionCode;
		
		public static List<String> readFile(String fileName){
			/**
			 * This method reads a file. 
			 * @param fileName is a String that is the name of the file. 
			 * 			it contains rows with 3 ints in each line. 
			 * @Exception FileNotFoundException if the file cannot be located. sets exception code to -1.
			 * @return a List<String> object in which each string is 1 line of the file.
			 * 			each string has an int for note, duration, and octave, plus can have spaces inbetween.
			 * 			
			 */	
			//***NEW added this exceptionCode =0; to properly initialize the exceptionCode
			exceptionCode =0;
			List<String> fileLineByLineList = new ArrayList<String>();  //create array list to hold lines
			try {
				Scanner scanner = new Scanner(new File(fileName));
				while(scanner.hasNextLine()) {
					
					fileLineByLineList.add(scanner.nextLine().trim());
				}
				scanner.close();
			}  //end of try
			catch (FileNotFoundException e) {
				exceptionCode = -1;
				e.printStackTrace();
			} //end of catch
			return fileLineByLineList;
		}  //end of readFile method
		
		public void play() throws MismatchException {
			//can define here
		}
	}  //end of abstract class PerformingArt
	
	class Music extends PerformingArt implements Art {
		
		//declare instance variables
		private String Composer;
		private String name;
		private ArrayList<Note> score;  //ArrayList of all the notes that comprise this music
		private int exceptionCode;
		
		
		//default Music constructor
		public Music() {
			/**
			 * This deafult constructor defines a Music object.
			 * @instanceVariable Composer holds a String with composer's name.
			 * @instanceVariable name holds String name of song
			 * @instanceVariable score is an empty ArrayList that could in the future hold Note objects. Notes make up a score, or song.
			 * @instanceVariable exceptionCode holds 0 for no exception, or -1, -2, or -3 to represent different types of exceptions thrown.
			 */
			Composer = null;
			name = null;
			score = new ArrayList<Note>();
			exceptionCode = 0;
		}
		
		//overloaded constructor
		public Music(String Composer, String name, ArrayList<Note> score) {
			/**
			 * This overloaded constructor defines a Music object.
			 * @instanceVariable Composer holds a String with composer's name.
			 * @instanceVariable name holds String name of song
			 * @instanceVariable score is an ArrayList of Note objects. Notes make up a score, or song.
			 * 	gets a score that we must deeply copy because of the composition relationship between Note and Music
			 *  Music Has-A Note (Music HAS Notes)
			 * @instanceVariable exceptionCode holds 0 for no exception, or -1, -2, or -3 to represent different types of exceptions thrown.
			 */
			this.Composer = Composer;
			this.name = name;
			this.exceptionCode = 0;
			this.score = new ArrayList<Note>();
			for (int i = 0; i < score.size(); i++) {
				this.score.add(new Note (score.get(i).getNote(), score.get(i).getDuration(), score.get(i).getOctave()));
			}	
		}
		

		public Music(String Composer, String name, List<String> score) {
			/**
			 * This overloaded constructor defines a Music object.
			 * @instanceVariable Composer holds a String with composer's name.
			 * @instanceVariable name holds String name of song
			 * @instanceVariable score is a List of String objects. Notes make up a score, or song.
			 * 	gets a score that we must deeply copy because of the composition relationship between Note and Music
			 *  Music Has-A Note (Music HAS Notes)
			 * //a list of strings, where each string contains 3 ints: a note, duration and octave. 
		     * //the List could have 1 string or multiple Strings; so one or more Notes.
			 * @instanceVariable exceptionCode holds 0 for no exception, or -1, -2, or -3 to represent different types of exceptions thrown.
			 */
			this.Composer = Composer;
			this.name = name;
			this.exceptionCode = 0;				
			this.score = new ArrayList<Note>();
			for (int i = 0; i < score.size(); i++) {  //for the number of Notes in the List
				ArrayList<Integer> oneLineOfScoreInts = new ArrayList<Integer>(); //create ArrayList to represent 1 Note
				String oneline = score.get(i).trim();  //create String to hold 1 String from the List
				                                       //the String resets for each new line in the List
				Scanner input = new Scanner(oneline);
				input.useDelimiter("[^0-9]+");   //use Scanner and Delimiter to get just the ints, remove spaces
				while (input.hasNextInt()) {
					oneLineOfScoreInts.add(input.nextInt()); //add the ints from just 1 String in the List to the ArrayList
				}
				
				//create a new Note with these 3 ints, and add the Note to the score. Repeat using for loop for all Strings in the List
				this.score.add(new Note(oneLineOfScoreInts.get(0), oneLineOfScoreInts.get(1), oneLineOfScoreInts.get(2)));
				input.close();  //close the Scanner, to avoid resource leak
			}//end of for size of score	
		} //end of music constructor method
		
		public String getName() {
			/**
			 * Getter method for private instance variable name.
			 * @return String name
			 */	
			String nameCopy = name;
			return nameCopy;
		}
		
		public String getComposer() {
			/**
			 * Getter method for private instance variable Composer.
			 * @return String Composer
			 */	
			String composerCopy = Composer;
			return composerCopy;
		}
		
		public int getExceptionCode() {
			/**
			 * Getter method for private instance variable exceptionCode.
			 * @return int exceptionCode
			 */	
			int code = exceptionCode;
			return code;
		}
		

		public ArrayList<Note> getScore() {
			/**
			 * Getter method for private instance variable score.
			 * @return ArrayList<Note> score
			 */	
			//for the composition relationship, return a deep copy of score
			ArrayList<Note> returnScore = new ArrayList<Note>();
			for (int i = 0; i < score.size(); i++) {
				returnScore.add(new Note (score.get(i).getNote(), score.get(i).getDuration(), score.get(i).getOctave()));
			}
			return returnScore;
		}
		
		public void setName(String name) {
			this.name = name;
		}
		
		public void setComposer(String Composer) {
			this.Composer = Composer;
		}
		
		public void setScore(ArrayList<Note> score) {
			this.score = new ArrayList<Note>();
			for (int i = 0; i < score.size(); i++) {
				this.score.add(new Note (score.get(i).getNote(), score.get(i).getDuration(), score.get(i).getOctave()));
			}
		}
		
		
		public void play() {
			//play in Note class plays Notes, not Music Objects. So, create for loop and pass each Note in the score to Note.play() one-by-one
			for (int i = 0; i < score.size(); i++) { 
				score.get(i).play();  //the get(i) gets each note in the score. play() will validate and if valid, will play.
			}	
		}
		
		public int compareTo(Art art) throws NullPointerException, ClassCastException {
			/**
			 * This method compares two music objects, however, the declared type could be Art instead of Music.
			 * @param art is an Art object, or subtype Music.
			 * @Exception NullPointerException is thrown if art of this is null, or if any of the items within art are null.
			 * @Exception ClassCastException thrown if art and this don't match. they must be Music or Art.
			 * @return 0 if the same name, composer and score. else return int -1.
			 */	
			try {
				if (art.equals(null) || this.equals(null)) throw new NullPointerException();
				
				//if they're not the same class, ClassCastException.
				if (art.getClass().equals(this.getClass()) == false) throw new ClassCastException();		
			
				//if art is a class other than note, this method shouldn't work. We can't have declared type music and actual type note, either.
				if (art.getClass().equals(Music.class) == false) throw new ClassCastException();

				//check if the individual parts of an art object are null...
				//check the Music object's name, composer, and score for NullPointerException
				//however, you can't pass a null object to this compareTo method, java will throw nullPointerexception itself
				if (art.getClass().equals(Music.class) == true) {
					Music typeart = (Music) art;
					if (typeart.name.equals(null) || this.name.equals(null)) throw new NullPointerException();
					if (typeart.Composer.equals(null) || this.Composer.equals(null)) throw new NullPointerException();
					if (typeart.score.equals(null) || this.score.equals(null)) throw new NullPointerException();

				}
			}//end of try

			catch (NullPointerException e) {
				exceptionCode = -1;
			}
			catch (ClassCastException e) {
				exceptionCode = -2;
			}
			
			//only continue if no exception yet thrown
			if (exceptionCode == 0) {
				String name = "";
				String composer = "";
				ArrayList<Note> score = null;
				
				//Make sure art is a Music object. Type cast it to Music, so we can get access to the object's name, composer and score.
				if (art.getClass().equals(Music.class) == true) {
					Music typeart = (Music) art;
					name = typeart.name;
					composer = typeart.Composer;
					score = typeart.score;
				}
				
				//if the two Music objects have the same name, composer and score 
				//-- same score meaning same length, and same arrayList entries in same order
				if (this.name.equalsIgnoreCase(name) && this.Composer.equalsIgnoreCase(composer) && this.score.size()==score.size()) {
					for (int i =0; i < score.size(); i++) {
						if (this.score.get(i).getDuration() != (score.get(i).getDuration()) || 
								this.score.get(i).getNote() != (score.get(i).getNote()) ||
									this.score.get(i).getOctave() != (score.get(i).getOctave())) {
							return -1;
						}
					}
					return 0;
				}
				
			} //end of if statement: only continue if no exception yet thrown
			
			//if 2 Music objects not equal
			return -1;
		}//end of compareTo		
	} //end of music class
	
	
	
	class Note extends PerformingArt implements Comparable<Art> {  //extends?
		
		private int note;
		private int duration;
		private int exceptionCode;
		private int octave;
		
		public Note() {
			/**
			 * This default constructor defines a Music object.
			 * @instanceVariable note holds an int with Note's note.
			 * @instanceVariable duration holds an int for a Note's duration.
			 * @instanceVariable octave holds an int for a Note's octave.
			 * @instanceVariable exceptionCode holds 0 for no exception, or -1, -2, or -3 to represent different types of exceptions thrown.
			 */
			note = 0;
			duration = 0;
			octave = 0;
			exceptionCode = 0;
		}
		
		public Note(int note, int duration, int octave) { //will check values in validate method
			/**
			 * This overloaded constructor defines a Music object.
			 * @instanceVariable note holds an int with Note's note.
			 * @instanceVariable duration holds an int for a Note's duration.
			 * @instanceVariable octave holds an int for a Note's octave.
			 * @instanceVariable exceptionCode holds 0 for no exception, or -1, -2, or -3 to represent different types of exceptions thrown.
			 */
			this.note = note;
			this.duration = duration;
			this.octave = octave;
			exceptionCode = 0;
		}
		

		public int compareTo(Art art) throws NullPointerException, ClassCastException {
			/**
			 * This method compares two Note objects, however, the declared type could be Art instead of Note.
			 * @param art is an Art object, or subtype Note.
			 * @Exception NullPointerException is thrown if art of this is null.
			 * @Exception ClassCastException thrown if art and this don't match. they must be Note or Art.
			 * @return 0 if the same octave and note. else compare just notes, and return the difference.
			 */	
			//should only be getting note objects
			//cannot compare music object to note object
			try {
				if (art.equals(null) || this.equals(null)) throw new NullPointerException();
				
				//if they're not the same class, ClassCastException.
				if (art.getClass().equals(this.getClass()) == false) throw new ClassCastException();		
			
				//if art is a class other than note, this method shouldn't work. We can't have declared type music and actual type note, either.
				if (art.getClass().equals(Note.class) == false) throw new ClassCastException();
				
				}//end of try

			catch (NullPointerException e) {
				exceptionCode = -1;
			}
			catch (ClassCastException e) {
				exceptionCode = -2;
			}
			
			
			int comparedNote = 0;
			int comparedOctave = 0;
			
			//**NEW ADDED, AFTER SUBMISSION, LOOKED AT TEST CASES
			//only continue if no exception yet thrown
			if (exceptionCode == 0) {
			
			//Make sure art is a Note object. Type cast it to Note, so we can get access to the object's note and octave
			if (art.getClass().equals(Note.class) == true) {
				Note typeOfArt = (Note) art;
				comparedNote = typeOfArt.note;
				comparedOctave = typeOfArt.octave;
			}

			//If art is a Note, compare octaves and notes to see if it's the same object
			if (this.octave == comparedOctave && this.note == comparedNote) {
				return 0;
			}
			
			} //end of if statement: only continue if no exception yet thrown
			
			//Else, they're not the same object, so compare note values only
//				int comparison = Integer.valueOf(this.note).compareTo(Integer.valueOf(note));
//				int comparison = Integer.valueOf(this.note) - Integer.valueOf(note);
//				comparison = java.lang.Math.abs(comparison);
				int comparison = this.note - comparedNote;
				return comparison;
			
	}
		
		
		public void play() {
			/**
			 * This method plays a Note. Uses synthesizer to make sound.
			 * @precondition first, before playing, will need to check if the given note is valid. Do this with validate() method.
			 * @Exception MismatchException is thrown if Note's note and octave don't match. or if duration negative.
			 * 
			 */	
			try {
				validate();  //make sure notes are valid before you play them
			}
			catch (MismatchException e) {  //id notes not valid, validate() will throw an exception, which will be caught and handled here
				exceptionCode = -3;
				System.out.println("Octave/Note Mismatch");
			}
			if (exceptionCode != -3) {  //if an exception was thrown, music will NOT be played
				try {
					Synthesizer midiSynthesizer = MidiSystem.getSynthesizer();
					midiSynthesizer.open();
					Instrument[] instrument = midiSynthesizer.getDefaultSoundbank().getInstruments();
					midiSynthesizer.loadInstrument(instrument[0]);
					MidiChannel[] midiChannels = midiSynthesizer.getChannels();
					midiChannels[0].noteOn(this.note, 100);
					try {
						Thread.sleep(duration);
					}
					catch (InterruptedException e) {
						e.printStackTrace();
					}
					midiChannels[0].noteOff(this.note);
				}
				catch (Exception e) {
					e.printStackTrace();
				}
			} //end of if (exceptionCode != -3)
	
		}
		
		private void validate() throws MismatchException {  
			/**
			 * This method validates a Note. Sees if the octave note, and duration are valid, and sees if they match.
			 * @Exception MismatchException is thrown if Note's note and octave don't match.
			 * also thrown if duration negative. or if octave or note outside valid range.
			 * 
			 */	
			try {
				if (note < 0 | note > 127) throw new MismatchException();  //not a valid note at all
				if (octave < -1 | octave > 9) throw new MismatchException();  //not a valid octave at all
				if (duration < 0 ) throw new MismatchException();  //duration must be positive
				
				//after above three lines, we know the note, octave, and duration are real, 
				//now see if note & octave match
				if (octave == -1) {
					if (note < 0 | note > 11) throw new MismatchException();
				}
				if (octave == 0) {
					if (note < 12 | note > 23) throw new MismatchException();
				}
				if (octave == 1) {
					if (note < 24 | note > 35) throw new MismatchException();
				}
				if (octave == 2) {
					if (note < 36 | note > 47) throw new MismatchException();
				}
				if (octave == 3) {
					if (note < 48 | note > 59) throw new MismatchException();
				}
				if (octave == 4) {
					if (note < 60 | note > 71) throw new MismatchException();
				}
				if (octave == 5) {
					if (note < 72 | note > 83) throw new MismatchException();
				}
				if (octave == 6) {
					if (note < 84 | note > 95) throw new MismatchException();
				}
				if (octave == 7) {
					if (note < 96 | note > 107) throw new MismatchException();
				}
				if (octave == 8) {
					if (note < 108 | note > 119) throw new MismatchException();
				}
				if (octave == 9) {
					if (note < 120 | note > 127) throw new MismatchException();
				}	
			} //end of try
			catch (MismatchException e) {
				System.out.println("Octave/Note Mismatch");
				exceptionCode = -3;
			} //end of catch	
		} //end of validate method


		public int getExceptionCode() {  //getter for a Note's exceptionCode
			int exceptionCodeCopy = exceptionCode;
			return exceptionCodeCopy;
		}

		public int getNote() {  //getter for a Note's note
			int noteCopy = note;
			return noteCopy;
		}

		public int getOctave() {  //getter for a Note's octave
			int octaveCopy = octave;
			return octaveCopy;
		}

		public int getDuration() {  //getter for a Note's duration
			int durationCopy = duration;
			return durationCopy;
		}
		
		public void setNote (int note) {
			this.note = note;
		}
		
		public void setOctave (int octave) {
			this.octave = octave;
		}
		
		public void setDuration (int duration) {
			this.duration = duration;
		}
		//no setter for exception code
	} //end of note class
	
	
	
//create the self-defined exception MismatchException
class MismatchException extends Exception{
	public MismatchException (){
		super();
	}
	public MismatchException(String message){
		super(message);
	}
}
