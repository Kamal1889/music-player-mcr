# Music Player 

Contains files PE2.java and music3.

music3 contains the notes, durations, and octaves.
These are the opening note's to My Chemical Romance's "Welcome to the Black Parade."

When you hit run on PE2.java, it reads music3 and
verifies the notes, durations, and octaves. Then it plays the music.

## Skills

This program demonstrates my excellent understanding of OOP concepts.
Uses interfaces, abstract classes, inheritance.
Uses static attributes, overloaded constructors. Does exception handling.

